import './App.css';
import { useSelector, useDispatch } from 'react-redux'
import { increment, decrement } from './redux/counterSlice';
function App() {

  const counter = useSelector((state) => state.counter.value)
  const dispatch = useDispatch()

  return (
    <div className="App">
        <button className='btn' id='increase' onClick={()=>dispatch(increment(counter + 1))}> + </button>
          {counter}
        <button className='btn' id='decrease' onClick={()=>dispatch(decrement(counter - 1))}> - </button>
    </div>
  );
}

export default App;
